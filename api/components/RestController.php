<?php
/**
 * Created by PhpStorm.
 * User: martrix
 * Date: 12.01.18
 * Time: 18:40
 */

namespace api\components;


use yii\rest\Controller;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\web\HttpException;
use yii\web\Response;
use Yii;


class RestController extends Controller
{
    /**
     * @var null
     */
    public $user = null;
    public $queryParams = [];

    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => HttpBearerAuth::className(),
                'except' => ['login', 'register'],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                ],
            ],

            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * @var array
     */
    public $requestData = [];

    public function beforeAction($action)
    {
        $this->requestData = Yii::$app->request->post(); // это если с curl
        //json_decode(file_get_contents("php://input"), true); // это если с JS
        $this->queryParams = Yii::$app->request->getQueryParams();
        return parent::beforeAction($action);
    }


    /**
     * Возвращаем ошибку
     * @param string $message Сообщение
     * @param int $errorCode Код ошибки
     * @param array $errorParams Параметры ошибки
     * @throws HttpException
     * @return \StdClass
     */
    public function sendError($message = '', $errorCode = 400, $errorParams = null)
    {
        throw new HttpException($errorCode, $message);
    }
}