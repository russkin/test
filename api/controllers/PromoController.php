<?php
/**
 * Created by PhpStorm.
 * User: martrix
 * Date: 12.01.18
 * Time: 18:51
 */

namespace api\controllers;

use api\components\RestController;
use common\models\Promo;

class PromoController extends RestController
{
    /**
     * @apiDefine myParam
     * @apiVersion 0.1.0
     * @apiParam {String} [fields] - список запрашиваемых полей разделенный запятыми id,code,date_start,date_end,tariff_zone,active,price
     */

    /**
     * @api {get} /promo?fields=:fields
     * @apiName  Index
     * @apiGroup Index
     * @apiDescription Получить список своих ключей
     * @apiVersion 0.1.0
     * @apiUse myParam
     * @apiSuccessExample {json} Success-Response type json:
     *    HTTP/1.1 200 OK
     * [
     * {
     * "id": 1,
     * "code": "qqqq",
     * "date_start": "12.12.2018",
     * "date_end": "12.12.2018",
     * "tariff_zone": "СПБ",
     * "active": "Активно",
     * "price": "500.00"
     * },
     * {
     * "id": 2,
     * "code": "www",
     * "date_start": "12.12.2018",
     * "date_end": "12.12.2019",
     * "tariff_zone": "СПБ",
     * "active": "Активно",
     * "price": "200.00"
     * }
     * ]
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 401 Server Error
     * {
     * "name": "Unauthorized",
     * "message": "Your request was made with invalid credentials.",
     * "code": 0,
     * "status": 401,
     * "type": "yii\\web\\UnauthorizedHttpException"
     * }
     */
    public function actionIndex($fields = null)
    {
        return Promo::getCodesForUser($fields);
    }

    /**
     * @api {post} /promo
     * @apiName  Create
     * @apiGroup Create
     * @apiDescription Создать новый код
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response type json:
     *    HTTP/1.1 200 OK
     * {
     * "id": 1000
     * }
     * @apiParamExample {json} $_POST Request-Example:
     *'{
     * "code"  :  "XXX",
     * "date_start"  :  "30.01.2018",
     * "date_end"  :  "30.01.2019",
     * "tariff_zone"  :  "1",
     * "price"  :  "222"
     * }'
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 401 Server Error
     * {
     * "name": "Unauthorized",
     * "message": "Your request was made with invalid credentials.",
     * "code": 0,
     * "status": 401,
     * "type": "yii\\web\\UnauthorizedHttpException"
     * }
     */
    public function actionCreate()
    {
        $model = new Promo();
        if ($model->load(['Promo' => $this->requestData]) and $model->save()) {
            return ['id' => $model->id];
        } else {
            \Yii::error('data');
            \Yii::error($this->requestData);
            return $this->sendError(json_encode(['errors' => $model->getErrors()]));
        }
    }

    /**
     * @api {get} /promo/:id/fields=:fields
     * @apiName  Get
     * @apiGroup Get
     * @apiDescription Получить ключ по id
     * @apiVersion 0.1.0
     * @apiUse myParam
     * @apiParam {integer} id id кода
     * @apiSuccessExample {json} Success-Response type json:
     *    HTTP/1.1 200 OK
     *
     * {
     * "id": 1,
     * "code": "qqqq",
     * "date_start": "12.12.2018",
     * "date_end": "12.12.2018",
     * "tariff_zone": "СПБ",
     * "active": "Активно",
     * "price": "500.00"
     * }
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 401 Server Error
     * {
     * "name": "Unauthorized",
     * "message": "Your request was made with invalid credentials.",
     * "code": 0,
     * "status": 401,
     * "type": "yii\\web\\UnauthorizedHttpException"
     * }
     */
    public function actionGet($id, $fields = 'id,code,date_start,date_end,tariff_zone,active,price')
    {
        if ($data = Promo::find()->where(['id' => $id])->forUser()->one()) {
            return Promo::prepareData($data->getAttributes(explode(',', $fields)));
        } else {
            return $this->sendError('Промокод не найден', 404);
        }

    }

    /**
     * @api {post} /promo/:id
     * @apiName  Update
     * @apiGroup Update
     * @apiDescription Изменить активированный ключ
     * @apiVersion 0.1.0
     * @apiParam {integer} id id кода
     * @apiSuccessExample {json} Success-Response type json:
     *    HTTP/1.1 200 OK
     * {
     * "id": 1000
     * }
     * @apiParamExample {json} $_POST Request-Example:
     *'{
     * "code"  :  "XXX",
     * "date_start"  :  "30.01.2018",
     * "date_end"  :  "30.01.2019",
     * "tariff_zone"  :  "1",
     * "price"  :  "222"
     * }'
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 401 Server Error
     * {
     * "name": "Unauthorized",
     * "message": "Your request was made with invalid credentials.",
     * "code": 0,
     * "status": 401,
     * "type": "yii\\web\\UnauthorizedHttpException"
     * }
     */
    public function actionUpdate($id)
    {
        if (!$model = Promo::find()->where(['id' => $id])->forUser()->active()->one()) {
            return $this->sendError('Промокод не найден', 404);
        }
        \Yii::error($this->requestData);
        if ($model->load(['Promo' => $this->requestData]) and $model->save()) {
            return ['id' => $id];
        } else {
            return $this->sendError(json_encode(['errors' => $model->getErrors()]));
        }

    }

    /**
     * @api {get} /promo/get-by-code?code=:code/fields=:fields
     * @apiName  GET
     * @apiGroup Get By Code
     * @apiDescription Получить ключ по коду
     * @apiVersion 0.1.0
     * @apiUse myParam
     * @apiParam {string} code  код
     * @apiSuccessExample {json} Success-Response type json:
     *    HTTP/1.1 200 OK
     *
     * {
     * "id": 1,
     * "code": "qqqq",
     * "date_start": "12.12.2018",
     * "date_end": "12.12.2018",
     * "tariff_zone": "СПБ",
     * "active": "Активно",
     * "price": "500.00"
     * }
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 401 Server Error
     * {
     * "name": "Unauthorized",
     * "message": "Your request was made with invalid credentials.",
     * "code": 0,
     * "status": 401,
     * "type": "yii\\web\\UnauthorizedHttpException"
     * }
     */
    public function actionGetByCode($code, $fields = 'date_start,date_end,tariff_zone,active,price')
    {
        if (!$model = Promo::find()->where(['code' => $code])->forUser()->one()) {
            return $this->sendError('Промокод не найден', 404);
        }
        return Promo::prepareData($model->getAttributes(explode(',', $fields)));
    }

    /**
     * @api {post} /promo/activate
     * @apiName  Activate
     * @apiGroup Activate
     * @apiDescription Активировать ключ
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response type json:
     *    HTTP/1.1 200 OK
     * 500
     * @apiParamExample {json} $_POST Request-Example:
     *'{
     * "code"  :  "XXX",
     * "tariff_zone"  :  "1"
     * }'
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 401 Server Error
     * {
     * "name": "Unauthorized",
     * "message": "Your request was made with invalid credentials.",
     * "code": 0,
     * "status": 401,
     * "type": "yii\\web\\UnauthorizedHttpException"
     * }
     */
    public function actionActivate()
    {
        if (!isset($this->requestData['code']) or !isset($this->requestData['tariff_zone'])) {
            return $this->sendError('Неверный формат данных', 400);
        }
        $where = ['code' => $this->requestData['code'], 'tariff_zone' => $this->requestData['tariff_zone']];

        if (!$model = Promo::find()->where($where)->notActive()->forUser()->one()) {
            return $this->sendError('Промокод не найден', 404);
        }
        $model->scenario = 'activate';
        $model->active = 1;
        if ($model->save()) {
            return $model->price;
        } else {
            return $this->sendError(json_encode(['errors' => $model->getErrors()]));
        }
    }

}