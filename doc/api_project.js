define({
    "name": "Тестовое задание",
    "version": "0.1.0",
    "description": "Просто тест .Авторизация по  header  Authorization: Bearer 123456  <br> Формат выдачи по умолчанию XML.<br> Для выдачи в JSON надо указать в header <b>Accept: application/json</b>",
    "title": "Документация API системы ",
    "url": "http://test.loc",
    "sampleUrl": "http://test.loc",
    "template": {
        "withCompare": false,
        "withGenerator": true
    },
    "defaultVersion": "0.0.0",
    "apidoc": "0.3.0",
    "generator": {
        "name": "apidoc",
        "time": "2018-01-12T19:28:58.167Z",
        "url": "http://apidocjs.com",
        "version": "0.17.6"
    }
});
