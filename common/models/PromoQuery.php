<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Promo]].
 *
 * @see Promo
 */
class PromoQuery extends \yii\db\ActiveQuery
{
    /**
     * Активные промокоды
     * @return $this
     */
    public function active()
    {
        return $this->andWhere('[[active]]=1');
    }

    /**
     * не активные промокоды
     * @return $this
     */
    public function notActive()
    {
        return $this->andWhere('[[active]]=0');
    }

    /**
     * промокоды текущего пользователя
     * @return $this
     */
    public function forUser()
    {
        return $this->andWhere(['user_id' => \Yii::$app->user->getId()]);
    }

    /**
     * @inheritdoc
     * @return Promo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Promo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
