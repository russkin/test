<?php

namespace common\models;

use Yii;


/**
 * This is the model class for table "promo".
 *
 * @property int $id
 * @property string $code
 * @property int $date_start
 * @property int $date_end
 * @property int $tariff_zone
 * @property int $active
 * @property int $user_id
 * @property User $user
 */
class Promo extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public static $tariffZone = ['Москва', 'СПБ', 'Казань', 'Краснодар', 'Ялта'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'date_start', 'date_end', 'tariff_zone'], 'required'],
            [['tariff_zone', 'active', 'user_id'], 'integer'],
            [['price'], 'number'],
            [['date_start', 'date_end'], 'safe'],
            [['code'], 'string', 'max' => 10],
        ];
    }

    public function beforeValidate()
    {
        if ($this->scenario != 'activate') {
            $this->date_start = strtotime($this->date_start);
            $this->date_end = strtotime($this->date_end);
        }
        $this->user_id = Yii::$app->user->getId();
        return parent::beforeValidate();
    }

    /**
     * Список кодов пользователя
     * @param array $fields
     * @return array
     */
    public static function getCodesForUser($fields)
    {
        $fields = ($fields) ? $fields : 'id,code,date_start,date_end,tariff_zone,active,price';
        $data = self::find()->forUser()->all();
        $result = [];
        foreach ($data as $row) {
            $item = $row->getAttributes(explode(',', $fields));
            $result[] = self::prepareData($item);
        }

        return $result;
    }

    public function scenarios()
    {
        $scenarios = ['activate' => ['active']];
        return array_merge($scenarios, parent::scenarios());
    }

    /**
     * подготовить данные для вывода
     * @param array $item
     * @return array
     */
    public static function prepareData($item)
    {

        foreach ($item as $key => $value) {

            switch ($key) {
                case 'date_start':
                    $item[$key] = date('d.m.Y', $value);
                    break;
                case 'date_end':
                    $item[$key] = date('d.m.Y', $value);
                    break;
                case  'tariff_zone':
                    $item[$key] = (isset(self::$tariffZone[$value])) ? self::$tariffZone[$value] : '';
                    break;
                case 'active':
                    $item[$key] = ($value) ? 'Активно' : 'Не активно';
                    break;
                case 'price':
                    $item[$key] = Yii::$app->formatter->asDecimal($value, 2);
                    break;
                default:
                    $item[$key] = $value;
            }
        }
        return $item;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'tariff_zone' => 'Tariff Zone',
            'active' => 'Active',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @inheritdoc
     * @return PromoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromoQuery(get_called_class());
    }
}
