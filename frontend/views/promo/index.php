<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PromoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Промо кода';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code',
            'price',
            [
                'attribute' => 'date_start',
                'format' => 'html',
                'value' => function ($model) {
                    return date('d.m.Y', $model->date_start);
                },
            ],
            [
                'attribute' => 'date_end',
                'format' => 'html',
                'value' => function ($model) {
                    return date('d.m.Y', $model->date_end);
                },
            ],
            [
                'attribute' => 'tariff_zone',
                'format' => 'html',
                'value' => function ($model) {
                    return \common\models\Promo::$tariffZone[$model->tariff_zone];
                },
            ],
            [
                'attribute' => 'active',
                'format' => 'html',
                'value' => function ($model) {
                    return ($model->active) ? 'Активен' : 'Не активен';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
