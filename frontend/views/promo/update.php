<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Promo */
$option = (!$model->active) ? ['disabled' => $model->active] : [];
$this->title = 'Update Promo: ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Promos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="promo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="promo-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'code')->textInput($option) ?>

        <?= $form->field($model, 'price')->textInput($option) ?>

        <?= $form->field($model, 'date_start')->textInput($option) ?>

        <?= $form->field($model, 'date_end')->textInput($option) ?>

        <?= $form->field($model, 'tariff_zone')->dropDownList(\common\models\Promo::$tariffZone, $option) ?>
        <?= $form->field($model, 'active')->dropDownList(['не активно', 'Активно']) ?>


        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
