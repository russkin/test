<?php

use yii\db\Migration;

/**
 * Class m180112_160558_user
 */
class m180112_160558_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('INSERT INTO `test`.`user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES (NULL, \'test\', \'123456\', \'\', NULL, \'test@site.com\', \'10\', \'\', \'\');');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180112_160558_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180112_160558_user cannot be reverted.\n";

        return false;
    }
    */
}
