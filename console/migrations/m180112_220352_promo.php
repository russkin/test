<?php

use yii\db\Migration;

/**
 * Class m180112_220352_promo
 */
class m180112_220352_promo extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE IF NOT EXISTS `promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `date_start` int(11) NOT NULL,
  `date_end` int(11) NOT NULL,
  `tariff_zone` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT \'0\',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;
');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180112_220352_promo cannot be reverted.\n";

        return false;
    }
    */
}
